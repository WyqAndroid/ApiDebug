package com.android.mainproject

import android.Manifest
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity

/**
 * 主项目相关代码示例
 */

class MainActivity : AppCompatActivity() {

    // true代表开启debug，只要在debug环境中启用日志模式
    val isDebug = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 获取读写文件权限
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 200)

        // 示例：Get请求，这里填写请求的url
        doGet("https://www.xxx.com/test/")

        // 示例：Post请求，这里填写请求的url
        val map = HashMap<String, String>()
        map["key"] = "value"
        map["key2"] = "value2"
        map["key3"] = "value3"
        doPost("https://www.xxx.com/test", map)
    }

    /**
     * POST接口数据请求
     * url：请求的接口链接
     * params：请求的接口参数
     */
    fun doPost(url: String, params: Map<String, String>): String {
        val result = ""

        // ....
        // do api request ...
        // ...

        if (isDebug) {
            LogUtil.saveLog(url, params.toString())
        }
        return result
    }

    /**
     * Get接口数据请求
     * url：请求的接口链接
     */
    fun doGet(url: String): String {
        val result = ""

        // ....
        // do api request ...
        // ...

        if (isDebug) {
            LogUtil.saveLog(url)
        }
        return result
    }

}
