package com.android.apishow.request.interfaces;

/**
 * 项目名称：<br>
 * 类名称：ApiRequest<br>
 * 类描述：接口请求类<br>
 * 创建人：汪勇奇<br>
 * 创建时间：2016年11月11日下午3:34:43<br>
 * 修改人： <br>
 * 修改时间： <br>
 * 修改备注：API请求结果回调
 *
 * @version V1.0
 */
public interface OnApiResultListener {

    void onResult(String result);

}
