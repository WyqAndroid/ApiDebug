package com.android.apishow

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.*
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.android.apishow.request.HttpClientHelper

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: ApiListAdapter
    private lateinit var popupWindow: PopupWindow
    private lateinit var searchAdapter: SearchListAdapter
    private var mRefreshAsyncTask: RefreshAsyncTask? = null
    private var progressDialog: ProgressDialog? = null

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        HttpClientHelper.initHttpClient(this)

        // 标题控件
        val tvTitle: TextView = findViewById(R.id.tv_title)
        // 列表控件
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        // 刷新控件
        val refreshLayout = findViewById<SwipeRefreshLayout>(R.id.swipe_refresh_layout)
        refreshLayout.isEnabled = false
        // 删除按钮
        val fabDelete = findViewById<FloatingActionButton>(R.id.fab_delete)
        // 切换按钮
        val tvSwitch = findViewById<TextView>(R.id.tv_switch)
        // 关键字搜索框
        val etSearch = findViewById<EditText>(R.id.et_search)
        // 头部布局
        val layoutHead = findViewById<View>(R.id.layout_head)
        // 初始化adapter
        adapter = ApiListAdapter(this)

        tvTitle.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        tvSwitch.visibility = View.VISIBLE
        etSearch.visibility = View.VISIBLE

        // 设置切换按钮点击监听
        tvSwitch.setOnClickListener {
            if (tvSwitch.text == "展示日志API") {
                adapter.isLogApi = true
                adapter.list = ApiList.instance.getLogList()
                adapter.notifyDataSetChanged()
                Toast.makeText(App.context, "一共找到 ${adapter.list.size}  条日志API数据", Toast.LENGTH_SHORT).show()
                refreshLayout.isEnabled = true
                etSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                fabDelete.visibility = View.VISIBLE
                tvSwitch.text = "展示默认API"
            } else {
                adapter.isLogApi = false
                adapter.list = ApiList.instance.getApiList()
                adapter.notifyDataSetChanged()
                Toast.makeText(App.context, "一共找到 ${adapter.list.size}  条默认API数据", Toast.LENGTH_SHORT).show()
                refreshLayout.isEnabled = false
                if (refreshLayout.isRefreshing) {
                    refreshLayout.isRefreshing = false
                }
                etSearch.visibility = View.VISIBLE
                tvTitle.visibility = View.GONE
                fabDelete.visibility = View.GONE
                tvSwitch.text = "展示日志API"
                if (mRefreshAsyncTask?.status == AsyncTask.Status.RUNNING) {
                    mRefreshAsyncTask?.cancel(true)
                    mRefreshAsyncTask = null
                }
            }
        }

        // 设置布局管理器
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        // 设置Item增加、移除动画
        recyclerView.itemAnimator = DefaultItemAnimator()
        // 设置adapter
        recyclerView.adapter = adapter
        // 设置数据源集合
        adapter.list = ApiList.instance.getApiList()
        // 更新适配器
        adapter.notifyDataSetChanged()
        // 设置点击监听
        adapter.setOnItemClickListener(object : OnListItemClickListener {
            override fun onListItemClickListener(api: Api) {
                val intent = Intent()
                intent.putExtra("name", api.name)
                intent.putExtra("type", api.type)
                intent.putExtra("url", api.url)
                intent.putExtra("parameter", api.parameter)
                intent.setClass(this@MainActivity, WebActivity::class.java)
                startActivity(intent)
            }
        })

        // 列表刷新
        refreshLayout.isEnabled = false
        refreshLayout.isRefreshing = false
        refreshLayout.setColorSchemeColors(Color.parseColor("#FF4081"))
        refreshLayout.setOnRefreshListener {
            if (tvSwitch.text == "展示默认API") {
                if (mRefreshAsyncTask == null || mRefreshAsyncTask?.status != AsyncTask.Status.RUNNING) {
                    mRefreshAsyncTask = RefreshAsyncTask(adapter, refreshLayout)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        mRefreshAsyncTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                    } else {
                        mRefreshAsyncTask?.execute()
                    }
                }
            }
        }

        // 删除按钮
        fabDelete.setOnClickListener {
            AlertDialog.Builder(this)
                    .setMessage("是否确定永久删除所有日志API数据？")
                    .setPositiveButton("确定") { _, _ ->
                        progressDialog = ProgressDialog.show(this, "", "正在删除中...", true)
                        progressDialog?.setCancelable(false)
                        Thread {
                            kotlin.run {
                                // 重新加载，防止没刷新拿不到所有数据
                                ApiList.instance.loadLogData()
                                val list = ApiList.instance.getLogList()
                                for (api in list) {
                                    // 删除文件
                                    try {
                                        api.file?.delete()
                                    } catch (ex: Exception) {
                                        Log.e("TAG", "file delete :" + ex.message)
                                    }
                                }
                                // 重新加载
                                ApiList.instance.loadLogData()
                                handler.sendEmptyMessage(200)
                            }
                        }.start()
                    }
                    .setNegativeButton("取消", null)
                    .create()
                    .show()
        }

        // 关键字搜索框监听
        etSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                // 进行搜索
                val text = etSearch.text
                if (!TextUtils.isEmpty(text)) {
                    val list = arrayListOf<Api>()
                    for (api in adapter.list) {
                        if (text in api.name) {
                            list.add(api)
                        }
                    }
                    if (list.isNotEmpty()) {
                        closePopupWindow()
                        showPopupWindow(layoutHead, list)
                    } else {
                        Toast.makeText(this, "匹配失败，请换个关键字再试试！", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            false
        }

        // 搜索窗口
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_search, null)
        val listView = view.findViewById<ListView>(R.id.lv_search)
        searchAdapter = SearchListAdapter(this)
        listView.adapter = searchAdapter
        searchAdapter.setOnItemClickListener(object : OnListItemClickListener {
            override fun onListItemClickListener(api: Api) {
                closePopupWindow()
                if (api.position >= 0) {
                    smoothMoveToPosition(recyclerView, api.position)
                }
            }
        })
        popupWindow = PopupWindow(view)
        popupWindow.width = ViewGroup.LayoutParams.MATCH_PARENT
        popupWindow.height = ViewGroup.LayoutParams.MATCH_PARENT
        popupWindow.isFocusable = true
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = false
        popupWindow.setBackgroundDrawable(BitmapDrawable(resources, Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)))

    }

    private var handler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            if (msg?.what == 200) {
                // 重新赋值
                adapter.list = ApiList.instance.getLogList()
                adapter.notifyDataSetChanged()
                if (progressDialog!!.isShowing) {
                    progressDialog?.dismiss()
                }
                Toast.makeText(App.context, "删除成功！", Toast.LENGTH_LONG).show()
            }
        }
    }

    // 异步任务
    private class RefreshAsyncTask : AsyncTask<String, String, String> {

        private var mAdapter: ApiListAdapter
        private var mRefreshLayout: SwipeRefreshLayout

        constructor(alAdapter: ApiListAdapter, refreshLayout: SwipeRefreshLayout) {
            mAdapter = alAdapter
            mRefreshLayout = refreshLayout
        }

        override fun doInBackground(vararg p0: String?): String {
            ApiList.instance.loadLogData()
            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            mAdapter.list = ApiList.instance.getLogList()
            mAdapter.notifyDataSetChanged()
            val count = mAdapter.list.size
            Toast.makeText(App.context, "一共找到 $count 条日志数据", Toast.LENGTH_SHORT).show()
            if (mRefreshLayout.isRefreshing) {
                mRefreshLayout.isRefreshing = false
            }
        }

    }

    /**
     * 显示关键字弹窗
     */
    private fun showPopupWindow(anchor: View, list: List<Api>) {
        if (popupWindow != null) {
            searchAdapter.list = list
            searchAdapter.notifyDataSetChanged()
            if (Build.VERSION.SDK_INT >= 24) {
                val location = IntArray(2)
                anchor.getLocationOnScreen(location)
                // 7.1 版本处理
                if (Build.VERSION.SDK_INT == 25) {
                    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
                    if (windowManager != null) {
                        val manager = this.windowManager
                        val outMetrics = DisplayMetrics()
                        manager.defaultDisplay.getMetrics(outMetrics)
                        val screenHeight = outMetrics.heightPixels
                        popupWindow.height = screenHeight - location[1] - anchor.height
                    }
                }
                popupWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, 0, location[1] + anchor.height)
            } else {
                popupWindow.showAsDropDown(anchor, 0, 0)
            }
        }
    }

    /**
     * 关闭关键字弹窗
     */
    private fun closePopupWindow() {
        if (popupWindow != null && popupWindow.isShowing) {
            popupWindow.dismiss()
        }
    }

    //记录目标项位置
    private var mToPosition: Int = 0
    //目标项是否在最后一个可见项之后
    private var mShouldScroll: Boolean = false

    /**
     * 滑动到指定位置
     */
    private fun smoothMoveToPosition(mRecyclerView: RecyclerView, position: Int) {
        // 第一个可见位置
        val firstItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(0))
        // 最后一个可见位置
        val lastItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(mRecyclerView.childCount - 1))
        if (position < firstItem) {
            // 第一种可能:跳转位置在第一个可见位置之前
            mRecyclerView.smoothScrollToPosition(position)
        } else if (position <= lastItem) {
            // 第二种可能:跳转位置在第一个可见位置之后
            val movePosition = position - firstItem
            if (movePosition >= 0 && movePosition < mRecyclerView.childCount) {
                val top = mRecyclerView.getChildAt(movePosition).top
                mRecyclerView.smoothScrollBy(0, top)
            }
        } else {
            // 第三种可能:跳转位置在最后可见项之后
            mRecyclerView.smoothScrollToPosition(position)
            mToPosition = position
            mShouldScroll = true
        }
    }

}
