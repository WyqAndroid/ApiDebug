package com.android.apishow

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/16.
 * 文件说明：列表点击监听
 */
interface OnListItemClickListener {

    // 列表点击
    fun onListItemClickListener(api: Api)

}