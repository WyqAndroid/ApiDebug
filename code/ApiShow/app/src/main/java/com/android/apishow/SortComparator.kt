package com.android.apishow

import android.text.TextUtils

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/19.
 * 文件说明：排序
 */

class SortComparator : Comparator<Api> {

    var mSort: String

    constructor(sort: String) {
        mSort = sort
    }

    override fun compare(l: Api, r: Api): Int {
        return compare(l.time, r.time)
    }

    private fun compare(left: Long, right: Long): Int {
        var state = 0
        if (!TextUtils.isEmpty(mSort)) {
            if (mSort == "asc") {
                // asc
                if (left > right) {
                    state = 1
                } else if (left < right) {
                    state = -1
                }
            } else {
                // desc
                if (left > right) {
                    state = -1
                } else if (left < right) {
                    state = 1
                }
            }
        }
        return state
    }

}