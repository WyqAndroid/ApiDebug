package com.android.apishow

import java.io.File

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/16.
 * 文件说明：Api实体类
 */

class Api {

    // 名称
    var name: String = ""
    // 类型
    var type: String = ""
    // 链接
    var url: String = ""
    // 参数
    var parameter: String = ""
    // 链接描述
    var description: String = ""

    // 文件
    var file: File? = null
    // 日期
    var date: String = ""
    // 时间戳
    var time: Long = 0
    // 位置
    var position: Int = 0

    override fun toString(): String {
        return "接口名称：" + name +
                "\n 接口类型：" + type +
                "\n 接口地址：" + url +
                "\n 接口参数：" + parameter +
                "\n 接口描述：" + description
    }

}